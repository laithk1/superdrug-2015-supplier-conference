﻿using System;
using System.IO;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;

namespace SignUp
{
    public class SignUpModule : NancyModule
    {
        public SignUpModule()
        {
            var baseFolder = HttpContext.Current.Server.MapPath("~/");
            var resultFile = string.Format("{0}_signups.csv", baseFolder);

            Post["/post"] = _ =>
            {
                var model = this.Bind<SignUpModel>();

                

                File.AppendAllText(resultFile, string.Format("{4},{0},{1},{2}{3}", model.FirstName, model.Surname, model.Email, Environment.NewLine, model.Attending ? "Attending" : "Not Attending"));

                if (model.Attending) {

                    return View["thankyou.html"];

                }

                    return View["thankyou_not_attending.html"];
            };

            Get["/"] = _ => View["signup.html"];

            Get["/_getsignups"] = _ =>
            {
                var file = new FileStream(resultFile,FileMode.Open);
                var response = new StreamResponse(() => file, MimeTypes.GetMimeType(resultFile));
            
                return response.AsAttachment("Superdrug_Signups.csv");
            };
        }

       
    }

    public class SignUpModel
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public bool Attending { get; set; }
    }
}